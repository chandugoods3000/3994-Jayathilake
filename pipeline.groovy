pipeline {
    agent any

    stages {
        stage('Build the docker Image') {
            steps {
                sh 'docker build -t my-docker-node-app .'
            }
        }

        stage('Start Container') {
            steps {
                sh 'docker run -d -p 3000:3000 my-docker-node-app'
            }
        }

        stage('Verify Deployment') {
            steps {
                sh 'docker ps'
            }
        }

    }
}
