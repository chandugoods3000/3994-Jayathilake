const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.json([
    {
      category: 'Book',
      title: 'The Name of the Wind',
      author: 'Patrick Rothfuss',
    },
    {
      category: 'Game',
      title: 'Hollow Knight',
      platform: 'Nintendo Switch',
    },
    {
      category: 'Show',
      title: 'Stranger Things',
      network: 'Netflix',
    },
  ]);
});

module.exports = app;
